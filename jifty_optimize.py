# Copyright(C) 2023 Max-Planck-Society
# SPDX-License-Identifier: GPL-2.0+ OR BSD-2-Clause

import jax
import sys
import pickle
import jax.numpy as jnp
import nifty8.re as jft

from functools import partial
from jax import random
from os import makedirs
from os.path import isfile

def optimize(lh, pos, n_iter, n_samples, linear_sampling_kwargs, non_linear_sampling_kwargs, newton_cg_kwargs, key, callback=None, out_dir=None, resume=False):
    if not out_dir == None:
        makedirs(out_dir, exist_ok=True)
    lfile = f"{out_dir}/last_finished_iteration"
    last_finished_index = -1
    if resume and isfile(lfile):
        with open(lfile) as f:
            last_finished_index = int(f.read())

    ham = jft.StandardHamiltonian(likelihood=lh).jit()
    @jax.jit
    def ham_vg(primals, primals_samples):
        assert isinstance(primals_samples, jft.kl.Samples)
        vvg = jax.vmap(jax.value_and_grad(ham))
        s = vvg(primals_samples.at(primals).samples)
        return jax.tree_util.tree_map(partial(jnp.mean, axis=0), s)


    @jax.jit
    def ham_metric(primals, tangents, primals_samples):
        assert isinstance(primals_samples, jft.kl.Samples)
        vmet = jax.vmap(ham.metric, in_axes=(0, None))
        s = vmet(primals_samples.at(primals).samples, tangents)
        return jax.tree_util.tree_map(partial(jnp.mean, axis=0), s)


    @partial(jax.jit, static_argnames=("point_estimates", ))
    def sample_evi(primals, key, *, point_estimates=()):
        # at: reset relative position as it gets (wrongly) batched too
        # squeeze: merge "samples" axis with "mirrored_samples" axis
        return jft.smap(
            partial(
                jft.sample_evi,
                lh,
                linear_sampling_name="S",  # enables verbose logging
                linear_sampling_kwargs=linear_sampling_kwargs,
                non_linear_sampling_kwargs=non_linear_sampling_kwargs,
                point_estimates=point_estimates,
            ),
            in_axes=(None, 0)
        )(primals, key).at(primals).squeeze()

    if last_finished_index > -1:
        pos = pickle.load(open(f"{out_dir}/position_it_{last_finished_index}.p", "rb"))
        key = pickle.load(open(f"{out_dir}/rnd_key{last_finished_index}.p", "rb"))
        if last_finished_index  == n_iter - 1:
            samples = pickle.load(open(f"{out_dir}/samples_{last_finished_index}.p", "rb"))
    else:
        pos = jft.Vector(pos.copy())

    for i in range(last_finished_index+1, n_iter):
        print(f"MGVI Iteration {i}", file=sys.stderr)
        print("Sampling...", file=sys.stderr)
        key, subkey = random.split(key, 2)

        samples = sample_evi(
            pos, random.split(subkey, n_samples)
        )

        print("Minimizing...", file=sys.stderr)
        opt_state = jft.minimize(
            None,
            pos,
            method="newton-cg",
            options={**{
                "fun_and_grad": partial(ham_vg, primals_samples=samples),
                "hessp": partial(ham_metric, primals_samples=samples),
                }, **newton_cg_kwargs}
        )
        pos = opt_state.x
        msg = f"Post MGVI Iteration {i}: Energy {ham_vg(pos, samples)[0]:2.4e}"
        print(msg, file=sys.stderr)
        if not callback == None:
            callback(pos, samples, i)
        if not out_dir == None:
            pickle.dump(pos, open(f"{out_dir}/position_it_{i}.p", "wb"))
            pickle.dump(samples, open(f"{out_dir}/samples_{i}.p", "wb"))
            pickle.dump(key, open(f"{out_dir}/rnd_key{i}.p", "wb"))
            with open(f"{out_dir}/last_finished_iteration", "w") as f:
                f.write(str(i))

    return pos, samples, key
